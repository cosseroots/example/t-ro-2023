# README

To run the simulation on your own computer, simply follow these steps

1. Clone repository
2. Add project and its dependencies to MATLAB's search path
3. Run simulation

## Clone Repository

```shell
$ git clone --recursive https://gitlab.univ-nantes.fr/cosseroots/example/t-ro-2023.git
```

## Update Repository

This repository is set up with several git submodules to freeze version numbers (and because MATLAB does not have any package manger...).
To update the repository to the newest version, run
```shell
$ git pull origin master
$ git submodule update --init --recursive
```

## Load Project and Dependencies

> If you do not have a MATLAB instance running, now would be a good time to start one.

Change your current working directory in MATLAB to the just-cloned directory.
Then run

```matlab
>> setup();
>> activate(MatlabProject.this());
```
to add all necessary paths to MATLAB's search path.


## Run Simulation

You can run the paper adjoining simulation by simply executing

```matlab
>> cd scripts/
>> flying_rod()
```
in the command window.
