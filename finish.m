function finish()
%% FINISH initializes this project
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Shut down this project



end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header
