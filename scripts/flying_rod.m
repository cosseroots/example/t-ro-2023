function flying_rod()
%% FLYING_ROD



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2022-01-16
% Changelog:
%   2022-01-16
%       * Initial release



%% Setup

% Create robot and simulation engine
rbt    = tro.robot();
engine = tro.engine();
% Assign the environment object to the simulation engine
engine.Environment = tro.Environment();

% Interval of temporal integration
engine.TimeSpan = [ 0.0 , 60.0 ];
% Temporal step size
engine.StepSize = 10 * 1e-3;

% Initial states for simulation
q0     = configurationHome(rbt);
q0dot  = zeros(rbt.VelocityNumber, 1);
q0ddot = zeros(rbt.VelocityNumber, 1);



%% Simulate

% Initialize engine
engine.SMMSTree = rbt;
engine.Q0       = q0;
engine.Q0dot    = q0dot;
engine.Q0ddot   = q0ddot;

% Simulate
while ~isDone(engine)
  step(engine);
end

% Display how long simulation took
fprintf('Simulation over %.2f s took %s', diff(engine.TimeSpan), time2str(totalTime(engine), 's'));

% Get results from simulation engine
[t, q, qdot, qddot] = result(engine);

% Get tracked signals from environment object
fext = engine.Environment.Signals.Data.Fext;
etaD = engine.Environment.Signals.Data.etaD;
etaE = engine.Environment.Signals.Data.etaE;

% And finally, store simulation result to file
mfile = tro.save(rbt, engine, t, q, qdot, qddot, fext, etaD, etaE);

% Animate from MAT file
tro.animate(mfile);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
