classdef Poolnoodle < Rod
  %% POOLNOODLE
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = Poolnoodle(varargin)
      %% POOLNOODLE
      
      
      narginchk(1, Inf);
      
      obj@Rod();
      
      d = 0.07;
      r = d / 2;
      A = pi * r ^ 2;
      J = diag(pi / 4 * r ^ 4 * [ 2 ; 1 ; 1 ]);
      
      E = 0.001 * 1e9;
      G  = shearmodulus(E, 1 / 4);

      rho = 950;
      
      obj.AllowedStrain        = [ 1 , 1 , 1 , 0 , 0 , 0 ];
      obj.BasisFunction        = LegendrePolynomial(2);
      obj.Length               = 1.0;
      obj.ReferenceStrain      = [ 0.0 , 0.0 , 0.0 , 1.0 , 0.0 , 0.0 ];
      
      obj.HookeanMatrix        = hookeanMatrix(A, J, E, G);
      obj.DampingMatrix        = 5 * 1e-3 * obj.HookeanMatrix;
      obj.SpatialInertiaMatrix = spatialInertiaMatrix(rho .* A, rho .* J, zeros(3, 1));
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
end
