classdef Environment < Environment
  %% ENVIRONMENT
  
  
  
  %% PUBLIC PROPERTIES
  properties ( Nontunable )
    
    Commander
    
    Controller
    
  end
  
  
  
  %% PUBLIC READ-ONLY PROPERTIES
  properties ( Nontunable , SetAccess = protected )
    
    % Stored signals
    Signals
    
  end
  
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = Environment(varargin)
      %% ENVIRONMENT
      
      
      
      % Commander
      obj.Commander = { ...
          SineWaveSignalGenerator( ...
              'Amplitude' , 1.0 * delta(6, 6) ...
            , 'Frequency' , 1 / 2 ...
            , 'Phase'     , 0.00 ...
          ) ...
        , [] ...
        , SineWaveSignalGenerator( ...
              'Amplitude' , 1.0 * delta(5, 6) ...
            , 'Frequency' , 1 / 4 ...
            , 'Phase'     , 0.00 ...
          ) ...
      };
      
      % Controller
      obj.Controller = { ...
          PIDControl( ...
            'Kp', [ 0.00 * ones(3, 1) ; 20.0 * ones(3, 1) ] ...
          , 'Ki', [ 0.00 * ones(3, 1) ; 25.0 * ones(3, 1) ] ...
        ) ...
        , [] ...
        , PIDControl( ...
            'Kp', [ 0.00 * ones(3, 1) ; 20.0 * ones(3, 1) ] ...
          , 'Ki', [ 0.00 * ones(3, 1) ; 25.0 * ones(3, 1) ] ...
        ) ...
      };
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% CONCRETE IMPLEMENTATION METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      nb = obj.Parent.SMMSTree.NumBody;
      
      % Loop over all bodies
      for ib = 1:nb
        % Set up controller?
        if ~isempty(obj.Controller{ib})
          % Set sampling rate of PID controller
          obj.Controller{ib}.Ts = obj.Parent.StepSize;
          
        end
        
      end
      
      obj.Signals = SignalTracker( ...
          'Name', { 'etaD' , 'etaE' , 'Fext' } ...
      );
      
    end
    
    
    function resetImpl(obj)
      %% RESETIMPL
      
      
      
      if isLocked(obj)
        reset(obj.Signals);
      end
      
    end
    
    
    function Fext = stepImpl(obj, t, q, qdot)
      %% STEPIMPL
      
      
      
      % Get tree object
      tree = obj.Parent.SMMSTree;
      nb = tree.NumBody;
      
      % Initialize output
      Fext   = zeros(6, nb);
      etaD   = zeros(6, nb);
      etaErr = zeros(6, nb);
      
      % Reconstruct pose of all bodies to reconstruct their forces
      [T, eta] = reconstructKinematics(tree, q, qdot);
      
      % Loop over all bodies that need to be controlled
      for ib = 1:nb
        % Is body controlled?
        if ~isempty(obj.Controller{ib})
          % Then calculate its control force
          [etaD(:,ib), etaErr(:,ib), Fext(:,ib)] = controlBody(obj, ib, t, T(:,:,ib), eta(:,ib));
          
        end
        
      end
      
      step(obj.Signals, etaD, etaErr, Fext);
      
    end
    
    
    function s = saveObjectImpl(obj)
      %% SAVEOBJECTIMPL
      
      
      
      s = saveObjectImpl@matlab.System(obj);
      
      if isLocked(obj)
        s.Signals = obj.Signals;
        
      end
      
    end
    
    
    function loadObjectImpl(obj, s, wasInUse)
      %% LOADOBJECTIMPL
      
      
      
      if wasInUse
        obj.Signals = matlab.System.loadObject(s.Signals);
        
      end
      
      loadObjectImpl@matlab.System(obj, s, wasInUse);
      
    end
    
  end
  
  
  
  %% INTERNAL METHODS
  methods ( Access = protected )
    
    function [etaD, etaErr, fext] = controlBody(obj, ind, t, Tj, etaj)
      %% CONTROLBODY
      
      
      
      % Get body object from tree
      bdy = obj.Parent.SMMSTree.Body{ind};
      
      % Get position of center of body
      Tb   = tformmul(Tj, bdy.Joint.BodyToJointTransform);
      etab = Ad(bdy.Joint.JointToBodyTransform) * etaj;
      
      % Get only the translational component of the body
      Td = trvec2tform(tform2trvec(Tb));
      
      % Transformation from desired to body frame
      Tbd = tformmul(tform2inv(Tb), Td);
      
      % Evalute new command value in desired coordinates and express in body's
      % coordinates.
      etaD = Ad(Tbd) * step(obj.Commander{ind}, t);
      
      % Error of world velocities expressed in body frame
      etaErr = etaD - etab;
      
      % Step through controller
      yCtrl = step(obj.Controller{ind}, etaErr);
      
      % Transform force into base coordinates
      fext = transpose(Ad(tform2inv(Tb))) * yCtrl;
      
    end
    
  end
  
end
