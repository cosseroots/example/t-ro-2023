function ngn = engine()
%% ENGINE Create the TRO simulation engine
%
% Inputs:
%
%   RBT                     Description of argument RBT
% 
%   ENV                     Description of argument ENV
%
% Outputs:
%
%   NGN                     Description of argument NGN
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ENGINE()
narginchk(0, 0);

% NGN = ENGINE(___)
nargoutchk(1, 1);



%% Algorithm

ngn = SimulationEngine();

ngn.StepSize = 20 * 1e-3;
ngn.TimeSpan = [ 0 , 10 ];

ngn.Solver = NewmarkBetaSolver();
ngn.Solver.Predictor = SO3R3Predictor();
ngn.Solver.Corrector = SO3R3Corrector();
ngn.Solver.Beta  = 1 / 4;
ngn.Solver.Gamma = 1 / 2;
ngn.Solver.MaxIterations     = 15;
ngn.Solver.FunctionTolerance = 1e-9;
ngn.Solver.StepTolerance     = 1e-9;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
