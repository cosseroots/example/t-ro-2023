function video(mfile, snaps)
%% VIDEO
%
% Inputs:
%
%   MFILE                   Description of argument MFILE
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse Arguments

% VIDEO(MFILE)
% VIDEO(MFILE, SNAPS)
narginchk(1, 2);

% VIDEO(___)
nargoutchk(0, 0);

% VIDEO(MFILE)
if nargin < 2 || isempty(snaps)
  snaps = [];
end



%% Animation

mp = MatlabProject.this();

[~, vfname, ~] = fileparts(mfile);
vfolder = data(mp, '%1$s', vfname);

% Create video writer object based on available profiles preferring MP4 over AVI
try
  vf = ensuredir(fullfile(sprintf(vfolder, 'video'), sprintf('%s.mp4', datestr8601())));
  vo = VideoWriter(vf, 'MPEG-4');
  
catch
  vf = ensuredir(fullfile(sprintf(vfolder, 'video'), sprintf('%s.avi', datestr8601())));
  vo = VideoWriter(vf, 'Motion JPEG AVI');
  
end
vo.FrameRate = 25;

tro.animate( ...
    mfile ...
  , 'StartFcn'  , @(ax, t_, q_) startFcn(ax, vo, t_, q_) ...
  , 'UpdateFcn' , @(ax, t_, q_) updateFcn(ax, vo, t_, q_) ...
  , 'StopFcn'   , @(ax, t_, q_) stopFcn(ax, vo, t_, q_) ...
);

end


function startFcn(ax, vo, t, q)
%% STARTFCN



% Open video writer object
open(vo);

end


function updateFcn(ax, vo, t, q)
%% UPDATEFCN



% Write frame to video
writeVideo(vo, getframe(ancestor(ax, 'figure')));


end


function stopFcn(ax, vo, t, q)
%% STOPFCN



% Close video writer
close(vo);


end


function start_cb(ax, rbt, vo, snaps, t, q, fext)
%% START_CB


tro.animate.startfcn(ax, rbt, t, q, fext);

viz.style.video(ax);

% Open video writer object for writing
open(vo);


end


function update_cb(ax, rbt, vo, snaps, t, q, fext)
%% UPDATE_CB


tro.animate.updatefcn(ax, rbt, t, q, fext);

% Write video frame
writeVideo(vo, getframe(ancestor(ax, 'figure')));

% Check if snapshot should be exported
if any(isclose(t, snaps))
  t_sec = floor(t);
  t_msec = round(1000 * ( t - t_sec ));
  ts = sprintf('%ds%03dms', t_sec, t_msec);
  
  % Create filename for export
  bname   = sprintf('snapshot_%s_%s.%%1$s', ts, datestr8601());
  
  % And export a snapshot
  tro.animate.exportsnapshot(ancestor(ax, 'figure'), fullfile(ax.UserData.VFile.folder, bname));
  
end


end


function stop_cb(ax, rbt, vo, snaps, t, q, fext)
%% STOP_CB


tro.animate.stopfcn(ax, rbt, t, q, fext);

% Export all body trajectories as standalone file
bname   = sprintf('bodytrajectory-spatial_%%2$02d_%s.%%1$s', datestr8601());
tro.animate.exportbodytrajectory(ancestor(ax, 'figure'), fullfile(ax.UserData.VFile.folder, bname));

% Close video writer
close(vo);

% Close figure to show we are done
close(ancestor(ax, 'figure'));


end


%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
