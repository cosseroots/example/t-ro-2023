function bodyTrajectory(mfile)
%% BODYTRAJECTORY
%
% Inputs:
%
%   MFILE                   Description of argument MFILE
% 
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% BODYTRAJECTORY(MFILE)
narginchk(1, 1);

% BODYTRAJECTORY(___)
nargoutchk(0, 0);



%% Initialization

mp = MatlabProject.this();

% Load data in a unified format
s = tro.load(mfile);

% Extract some data from MAT file
rbt   = s.smms;
% ngn   = s.engine;

t = s.result.t;
q = s.result.q;

[~, bname, ~] = fileparts(fullpath(mfile));
% Append current date-time to the filename so we can have multiple versions of
% each video
bfolder = data(mp, '%1$s', bname);




%% Styling

% Number of...
nb = rbt.NumBody;
nt = numel(t);

% Reconstruct all states
T = cell(1, nt);
parfor it = 1:nt
  T{it} = reconstructKinematics(rbt, q(:,it));
end
T = cat(4, T{:});

% Name of coordinates
coords = {'x', 'y', 'z'};
% Default line style order
lso = { '-' , '--' , '-.' };
% Colors of each body
c = [ ...
  color('red')   ; ...
  color('green') ; ...
  color('blue')  ; ...
];
bind = [ 1 , 4 , 7 ];
bind = [ 0 , 3 , 6 ];

% Figure and axes for all positions in one
hfa = figure();
hax = axes('Parent', hfa);
coFig = onCleanup(@() close(hfa));

% Loop over each body
for ib = 1:nb
  hf_ = figure();
  coFig_ = onCleanup(@() close(hf_));
  
  hax_ = axes('Parent', hf_);
  hax_.NextPlot = 'add';
  
  % Plot body
  p = tform2trvec(squeeze(T(:,:,ib,:)));
  hp = gobjects(1, 3);
  % Plot each coordinate
  for ic = 1:numel(coords)
    hp(ic) = plot( ...
        hax_ ...
      , t, p(ic,:) - p(ic,1) ...
      , 'Color'       , c(ib,:) ...
      , 'LineStyle'   , lso{ic} ...
      , 'DisplayName' , sprintf('$ \\mathcal{ B }_{ %0.f , %s } $', bind(ib), coords{ic}) ...
    );
    
  end
  
  if ib == nb
    viz.style.timeseries(hax_);
  end
  
  hax_.YAxis.Label.String = '$ \Delta \left[ \mathrm{m} \right] $';
  hax_.XAxis.Limits = t([1,end]);
  hax_.YAxis.Limits = [ -0.50 , +1.50 ];
  
  copyobj(hp, hax);
  
  fbaspect(hf_, [3, 1]);
  textinterpreters(hf_, 'latex');
  
  % Update drawing to safely export it
  drawnow();
  
  % Name where to save file to
  hfname = sprintf('bodytrajectory_%02d_%s.%%1$s', ib, datestr8601());
  fpath = fullfile(bfolder, hfname);
  
  tro.publish.savefig(hf_, fpath, 'T-RO Figure');
  
end



%% All trajectories into one

% Export the big figure
legend( ...
    hax ...
  , hax.Children(1:3:end) ...
  , arrayfun(@(ib) sprintf('$ \\mathcal{ B }_{ %0.f } $', ib), bind, 'UniformOutput', false) ...
  , 'Location', 'best' ...
);

hax.XAxis.Label.String = hax_.XAxis.Label.String;
hax.YAxis.Label.String = hax_.YAxis.Label.String;

textinterpreters(hfa, 'latex');
fbaspect(hfa, [3, 1]);

% Update drawing to safely draw it
drawnow();

% Name where to save file to
hfname = sprintf('bodytrajectory_all_%s.%%1$s', datestr8601());
fpath = fullfile(bfolder, hfname);


tro.publish.savefig(hfa, fpath, 'T-RO Figure');



%% All trajectories per coordinate

% Loop over each coordinate
for ic = 1:numel(coords)
  hf_ = figure();
  coFig_ = onCleanup(@() close(hf_));
  
  hax_ = axes('Parent', hf_);
  hax_.NextPlot = 'add';
   
  hp = gobjects(1, nb);
  
  % Loop over each body
  for ib = 1:nb
    % Plot body
    p = tform2trvec(squeeze(T(:,:,ib,:)));
    
    hp(ib) = plot( ...
        hax_ ...
      , t, p(ic,:) - p(ic,1) ...
      , 'Color'      , c(ib,:) ...
      , 'LineStyle'  , lso{ib} ...
      , 'DisplayName', sprintf('$\\mathcal{B}_{%d}$', bind(ib)) ...
    );

  end
  
  if ic == numel(coords)
    viz.style.timeseries(hax_);
  end
  
  hax_.YAxis.Label.String = '$\Delta \left[ \mathrm{m} \right]$';
  hax_.XAxis.Limits = t([1,end]);
  hax_.YAxis.Limits = [ -0.50 , +1.50 ];

  if ic == 1
    legend(hax_, 'Location', 'best');
  end
  
  fbaspect(hf_, [3, 1]);
  textinterpreters(hf_, 'latex');
  
  % Update drawing to safely export it
  drawnow();
  
  % Name where to save file to
  hfname = sprintf('bodytrajectory_c%s_%s.%%1$s', coords{ic}, datestr8601());
  fpath = fullfile(bfolder, hfname);

  tro.publish.savefig(hf_, fpath, 'T-RO Figure');
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
