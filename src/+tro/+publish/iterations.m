function iterations(mfile)
%% ITERATIONS
%
% Inputs:
%
%   MFILE                   Description of argument MFILE
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ITERATIONS(MFILE)
narginchk(1, 1);

% ITERATIONS(___)
nargoutchk(0, 0);



%% Algorithm

% Load data in a unified format
s = tro.load(mfile);

% Extract some data from MAT file
stats = statistics(s.engine);
iters = [ stats.iteration ];
t     = s.result.t(1:numel(iters));

% New figure
hf = figure();
coFig = onCleanup(@() close(hf));
hax = axes('Parent', hf);
hold(hax, 'on');

% Iterations
stairs( ...
    hax ...
  , t, iters ...
  , 'LineWidth', 1.0 ...
);

% Max iterations
yline( ...
    hax ...
  , s.engine.Solver.MaxIterations ...
  , 'Color', color('DarkGray') ....
  , 'LineStyle', '--' ...
  , 'LineWidth', 0.75 ...
);

% Adjust axes
viz.style.timeseries(hax);
hax.XAxis.Limits = t([1,end]);

% Y-Axis
hax.YAxis.Label.String = '\# Iterations';
hax.YAxis.Limits       = [ 0 , s.engine.Solver.MaxIterations + 1];
hax.YAxis.TickValues   = sort(unique([ 0 , hax.YAxis.Limits(1):5:hax.YAxis.Limits(2) , s.engine.Solver.MaxIterations ]));

% Adjust all text interpreters
textinterpreters(hax, 'latex');

% Update drawing, of course
drawnow();



%% Export

mp = MatlabProject.this();

% Prepare export path
[~, ffname, ~] = fileparts(mfile);

% Full file name of figure
hfname = sprintf('iterations_%s.%%1$s', datestr8601());
fpath = data(mp, '%1$s', ffname, hfname);

% Export data as DAT file
tbl = transpose(cat(1, t, double(iters)));
hdr = {'t', 'iter'};
datfile = ensuredir(sprintf(fpath, 'dat'));
writecell( ...
    hdr ...
  , datfile ...
  , 'Delimiter', ',' ...
);
dlmwrite( ...
    datfile ...
  , tbl ...
  , '-append' ...
  , 'Delimiter', ',' ...
  , 'Precision', '%.3f' ...
);

% Set figure's aspect ratio
fbaspect(hf, [ 3 , 1 ]);

% Finally, export figure to file
hgexport( ...
    hf ...
  , 'temp_dummy' ...
  , hgexport('readstyle', 'T-RO Figure') ...
  , 'applystyle', true ...
);

tro.publish.savefig(hf, fpath);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
