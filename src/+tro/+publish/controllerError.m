function controllerError(mfile)
%% CONTROLLERERROR
%
% Inputs:
%
%   MFILE                   Description of argument MFILE
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% CONTROLLERERROR(MFILE)
narginchk(1, 1);

% CONTROLLERERROR(___)
nargoutchk(0, 0);



%% Initialization

% Load data in a unified format
s = tro.load(mfile);

% Plots per component
in_percomponent(mfile, s);

% Plot of error norms
in_errornorms(mfile, s);

% Plot of errors: X==T, Y==Coord, Z==Error
in_onplanes(mfile, s);


end


function name = bind2bname(ind)
%% BIND2BNAME


persistent c_

if isempty(c_)
  c_ = [ 1 , 4 , 7 ];
end

name = c_(ind);


end


function c = principalaxis(ind)
%% PRINCIPALAXIS


persistent c_

if isempty(c_)
  c_ = { 'x' , 'y' , 'z' };
end

if nargin > 0
  c = c_{ind};
  
else
  c = c_;
  
end


end


function b = bfolder(mfile)
%% BFOLDER


[b_, bname, ~] = fileparts(fullpath(mfile));
b = fullpath(fullfile(b_, '..', '%1$s', bname));


end


function in_percomponent(mfile, s)
%% IN_PERCOMPONENT


% Extract some data from MAT file
rbt = s.smms;
ngn = s.engine;
env = ngn.Environment;

t = s.result.t;
nt = numel(t);

etaE = s.result.etaE;

% New figure for all plots
hf = figure();
ht = tiledlayout( ...
    'flow' ...
  , 'Parent'     , hf ...
  , 'TileSpacing', 'compact' ...
);
coFig = onCleanup(@() close(hf));

ll = zeros(1, 2);
al = zeros(1, 2);

% Loop over each body to plots its error
for ib = 1:rbt.NumBody
  % Skip if there was no controller on the body
  if isempty(env.Controller{ib})
    continue
  end
  
  % Create a new axes
  hax = nexttile(ht);
  % Get error and strip down to number of time steps
  e = permute(etaE(:,ib,:), [1, 3, 2]);
  
  % Linear error
  el = twist2lin(e);
  yyaxis('left');
  hold(hax, 'on');
  hp = gobjects(1, 3);
  for ic = 1:3
    hp(ic) = plot( ...
        hax ...
      , t, el(ic,:) ...
      , 'DisplayName', sprintf('$V_{ \\mathrm{%s} }$', principalaxis(ic)) ...
    );
  end
  [~, hll] = plot_markers( ...
      hp, 11 ...
    , 'Spacing'   , 'curve' ...
    , 'Order'     , { 'o' , '+' , '*' } ...
    , 'MarkerSize', 4 ...
  );
  hold(hax, 'off');
  [~, yr_] = axisdatarange(hax);
  ll = minmax([ll, yr_]);
  
  % Angular error
  ea = twist2ang(e);
  yyaxis('right');
  hold(hax, 'on');
  hp = gobjects(1, 3);
  for ic = 1:3
    hp(ic) = plot( ...
        hax ...
      , t, ea(ic,:) ...
      , 'DisplayName', sprintf('$\\Omega_{ \\mathrm{%s} }$', principalaxis(ic)) ...
    );
  end
  [~, hla] = plot_markers( ...
      hp, 7 ...
    , 'Spacing'   , 'curve' ...
    , 'Order'     , { 'o' , '+' , '*' } ...
    , 'MarkerSize', 4 ...
  );
  hold(hax, 'off');
  [~, yr_] = axisdatarange(hax);
  al = minmax([al, yr_]);
  
  % Configure axes
%   hax.Title.String = sprintf('Error SE(3) Body $\\mathcal{B}_{%d}$', bind2bname(ib));
  
  viz.style.timeseries(hax);
  hax.XAxis.Limits = t([1,end]);
  hax.YAxis(1).Label.String = '$e_{ V } \left[ \mathrm{m} / \mathrm{s} \right]$';
  hax.YAxis(2).Label.String = '$e_{ \Omega } \left[ \mathrm{rad} / \mathrm{s} \right]$';
  
  legend(hax, [hll , hla], 'Location', 'best');
  
end

% Clean up axes and remove X-axis labels from all but the last Axis
haxx = findobj(hf,  'type', 'axes');

% Get data range to properly adjust axes' limits
yax = [ haxx.YAxis ];
% Left-hand ...
yaxl = yax(1,:);
% ... and right-hand axes
yaxr = yax(2,:);

set( ...
    yaxl ...
  , 'Limits' ...
  , [ ... 
      round(floor(ll(1) / 0.5)) * 0.5 ...
    , round( ceil(ll(2) / 0.5)) * 0.5 ...
  ] ...
);
set( ...
    yaxr ...
  , 'Limits' ...
  , [ ... 
      round(floor(al(1) / 0.5)) * 0.5 ...
    , round( ceil(al(2) / 0.5)) * 0.5 ...
  ] ...
);

% Remove legend in all but the first legend
legend(haxx(1:(end-1)), 'off');
hl = get(haxx(end), 'Legend');
hl.ItemTokenSize  = hl.ItemTokenSize .* [ 0.5 ; 0.75 ];
hl.NumColumnsMode = 'auto';
hl.Orientation    = 'horizontal';
hl.LineWidth      = 0.70;
hl.Layout.Tile    = 'north';

% Remove X-Labels in all but the last axes
xlabel(haxx(2:end), '');

% Prepare figure for export
textinterpreters(hf, 'latex');
fbaspect(hf, [3, 2]);
drawnow();

% Name where to save file to
hfname = sprintf('controller-error_%s.%%1$s', datestr8601());
fpath = fullfile(bfolder(mfile), hfname);

tro.publish.savefig(hf, fpath, 'T-RO Figure');


end


function in_errornorms(mfile, s)
%% IN_ERRORNORMS


% Extract some data from MAT file
rbt   = s.smms;
ngn   = s.engine;
env   = ngn.Environment;

t    = s.result.t;
etaE = s.result.etaE;
nt = numel(t);

% New figure for all plots
hf = figure();
ht = tiledlayout( ...
    'flow' ...
  , 'Parent'      , hf ...
  , 'TileSpacing' , 'compact' ...
);
coFig = onCleanup(@() close(hf));

ll = zeros(1, 2);
al = zeros(1, 2);

% Loop over each body to plots its error
for ib = 1:rbt.NumBody
  % Skip if there was no controller on the body
  if isempty(env.Controller{ib})
    continue
  end
  
  % Create a new axes
  hax = nexttile(ht);
  % Get error and strip down to number of time steps
  e = permute(etaE(:,ib,:), [1, 3, 2]);
  
  % Linear error
  el = twist2lin(e);
  yyaxis('left');
  hold(hax, 'on');
  hp = plot( ...
      hax ...
    , t, vecnorm(el, 2, 1) ...
    , 'DisplayName', '$|| e_{ V } ||$' ...
  );
  [~, hll] = plot_markers( ...
      hp, 11 ...
    , 'Spacing'   , 'curve' ...
    , 'Order'     , { 'o' , '+' , '*' } ...
    , 'MarkerSize', 4 ...
  );
  hold(hax, 'off');
  [~, yr_] = axisdatarange(hax);
  ll = minmax([ll, yr_]);
  
  % Angular error
  ea = twist2ang(e);
  yyaxis('right');
  hold(hax, 'on');
  hp = plot( ...
      hax ...
    , t, vecnorm(ea, 2, 1) ...
    , 'DisplayName', '$|| e_{ \Omega } ||$' ...
  );
  [~, hla] = plot_markers( ...
      hp, 7 ...
    , 'Spacing'   , 'curve' ...
    , 'Order'     , { '*' , 'o' , '+' } ...
    , 'MarkerSize', 4 ...
  );
  hold(hax, 'off');
  [~, yr_] = axisdatarange(hax);
  al = minmax([al, yr_]);
  
  % Configure axes
%   hax.Title.String = sprintf('Error SE(3) Body $\\mathcal{B}_{%d}$', bind2bname(ib));
  
  viz.style.timeseries(hax);
  hax.XAxis.Limits = t([1,end]);
  hax.YAxis(1).Label.String = '$|| e_{ V } || \left[ \mathrm{m} / \mathrm{s} \right]$';
  hax.YAxis(2).Label.String = '$|| e_{ \Omega } || \left[ \mathrm{rad} / \mathrm{s} \right]$';
  
  legend(hax, [hll , hla], 'Location', 'best');
  
end

% Clean up axes and remove X-axis labels from all but the last Axis
haxx = findobj(hf,  'type', 'axes');

% Get data range to properly adjust axes' limits
yax = [ haxx.YAxis ];
% Left-hand ...
yaxl = yax(1,:);
% ... and right-hand axes
yaxr = yax(2,:);
m = max(abs(ll));
set( ...
    yaxl ...
  , 'Limits' ...
  , [ 0 , +1 ] * ceil(m) ...
);
m = max(abs(al));
set( ...
    yaxr ...
  , 'Limits' ...
  , [ 0 , +1 ] * ceil(m / 2) * 2 ...
);

% Remove legend in all but the first legend
legend(haxx(1:(end-1)), 'off');
hl = get(haxx(end), 'Legend');
hl.ItemTokenSize  = hl.ItemTokenSize .* [ 0.5 ; 0.75 ];
hl.NumColumnsMode = 'auto';
hl.Orientation    = 'horizontal';
hl.LineWidth      = 0.70;
hl.Layout.Tile    = 'north';

% Remove X-Labels in all but the last axes
xlabel(haxx(2:end), '');

% Prepare figure for export
textinterpreters(hf, 'latex');
fbaspect(hf, [3, 2]);
drawnow();

% Name where to save file to
hfname = sprintf('controller-error-norm_%s.%%1$s', datestr8601());
fpath = fullfile(bfolder(mfile), hfname);

tro.publish.savefig(hf, fpath, 'T-RO Figure');


end


function in_onplanes(mfile, s)
%% IN_ONPLANES


% Extract some data from MAT file
rbt = s.smms;
ngn = s.engine;
env = ngn.Environment;

t  = s.result.t;
nt = numel(t);

etaE = s.result.etaE;

c = [ ...
  rgbdarker(color('red')  , 0.10) ; ...
  rgbdarker(color('green'), 0.20) ; ...
  rgbdarker(color('blue') , 0.10) ; ...
];

% New figure for all plots
hf = figure();
ht = tiledlayout( ...
    'flow' ...
  , 'Parent'      , hf ...
  , 'TileSpacing' , 'compact' ...
);
coFig = onCleanup(@() close(hf));

% Loop over each body to plots its error
for ib = 1:rbt.NumBody
  % Skip if there was no controller on the body
  if isempty(env.Controller{ib})
    continue
  end
  
  % Create a new axes
  hax = nexttile(ht);
  % Get error and strip down to number of time steps
  e = permute(etaE(:,ib,:), [1, 3, 2]);
  
  % Linear error
  el = twist2lin(e);
  hold(hax, 'on');
  Y = -ones(size(t));
  for ic = 1:3
    plot3( ...
        hax ...
      , t ...
      , ic .* Y ...
      , el(ic,:) ...
      , 'DisplayName' , sprintf('$e_{ V_{ \\mathrm{%s} } }$', principalaxis(ic)) ...
      , 'Color'       , c(ic,:) ...
    );
  end
  viewiso(hax);
  
  % Configure axes
%   hax.Title.String = sprintf('Error SE(3) Body $\\mathcal{B}_{%d}$', bind2bname(ib));
  
  hax.DataAspectRatio    = [ 1 , 1.5 , 1 ];
  hax.PlotBoxAspectRatio = [ 10 , 4 , 1 ];
  
  viz.style.timeseries(hax);
  hax.XAxis.Limits = t([1,end]);
  
%   hax.YAxis.Label.String = 'Coordinate';
  hax.YAxis.TickLabels = principalaxis();
  s = sign(Y);
  hax.YAxis.TickValues = sort(s(1) * (1:3));
  hax.YAxis.Limits = minmax(hax.YAxis.TickValues) + [ -0.1, +0.1];
  
  hax.ZAxis.Label.String = '$e_{ V } \left[ \mathrm{m} / \mathrm{s} \right]$';
  
  hax.View = [ 30 , 5 ];
  
end

% Make axes limits the same for all axes
haxx = findobj(hf, 'type', 'axes');

xl = minmax(cat(2, haxx.XLim));
yl = minmax(cat(2, haxx.YLim));
zl = minmax(cat(2, haxx.ZLim));

nexttick = 0.75;

set( ...
    [haxx.XAxis] ...
  , 'Limits' ...
  , t([ 1 , end ]) ...
  , 'TickValues', t(1):2:t(end) ...
);
set( ...
    haxx ...
  , 'ZLim' ...
  , [ ...
      round(floor(zl(1) / nexttick)) * nexttick ...
    , round( ceil(zl(2) / nexttick)) * nexttick ...
  ] ...
);

% Prepare figure for export
textinterpreters(hf, 'latex');
fbaspect(hf, [3, 2]);
drawnow();

% Name where to save file to
hfname = sprintf('controller-error_linear-evolution_%s.%%1$s', datestr8601());
fpath = fullfile(bfolder(mfile), hfname);

tro.publish.savefig(hf, fpath, 'T-RO Figure');



%% Angular

% New figure for all plots
hf = figure();
ht = tiledlayout( ...
    'flow' ...
  , 'Parent'     , hf ...
  , 'TileSpacing', 'compact' ...
);
coFig = onCleanup(@() close(hf));

% Loop over each body to plots its error
for ib = 1:rbt.NumBody
  % Skip if there was no controller on the body
  if isempty(env.Controller{ib})
    continue
  end
  
  % Create a new axes
  hax = nexttile(ht);
  % Get error and strip down to number of time steps
  e = permute(etaE(:,ib,:), [1, 3, 2]);
  
  % Linear error
  el = twist2ang(e);
  hold(hax, 'on');
  Y = -ones(size(t));
  for ic = 1:3
    plot3( ...
        hax ...
      , t ...
      , ic .* Y ...
      , el(ic,:) ...
      , 'DisplayName' , sprintf('$ e_{ \\Omega_{ \\mathrm{%s} } } $', principalaxis(ic)) ...
      , 'Color'       , c(ic,:) ...
    );
  end
  viewiso(hax);
  
  % Configure axes
%   hax.Title.String = sprintf('Error SE(3) Body $\\mathcal{B}_{%d}$', bind2bname(ib));
  
  hax.DataAspectRatio    = [ 1 , 1.5 , 5 ];
  hax.PlotBoxAspectRatio = [ 10 , 4 , 1 ];
  
  viz.style.timeseries(hax);
  hax.XAxis.Limits = t([1,end]);
  
%   hax.YAxis.Label.String = 'Coordinate';
  hax.YAxis.TickLabels = principalaxis();
  s = sign(Y);
  hax.YAxis.TickValues = sort(s(1) * (1:3));
  hax.YAxis.Limits = minmax(hax.YAxis.TickValues) + [ -0.1, +0.1];
  
  hax.ZAxis.Label.String = '$ e_{ \Omega } \left[ \mathrm{rad} / \mathrm{s} \right] $';
  
  hax.View = [ 30 , 5 ];
  
end

% Make axes limits the same for all axes
haxx = findobj(hf, 'type', 'axes');

xl = minmax(cat(2, haxx.XLim));
yl = minmax(cat(2, haxx.YLim));
zl = minmax(cat(2, haxx.ZLim));

set( ...
    [haxx.XAxis] ...
  , 'Limits' ...
  , t([ 1 , end ]) ...
  , 'TickValues', t(1):2:t(end) ...
);
nexttick = pi;
nzl = [ ...
      round(floor(zl(1) / nexttick)) * nexttick ...
    , round( ceil(zl(2) / nexttick)) * nexttick ...
  ];
set( ...
    [haxx.ZAxis] ...
  , 'Limits'    , nzl ...
  , 'TickValues', (nzl(1):pi:nzl(2)) ...
);
tvs = haxx(1).ZAxis.TickValues;
tvl = arrayfun( ...
      @(tv) sprintf('$%d \\pi$', round(tv)) ...
    , tvs ./ pi ...
    , 'UniformOutput', false ...
);
tvl(isclose(tvs, 0))   = { '$0$' };
tvl(isclose(tvs, -pi)) = { '$-\pi$' };
tvl(isclose(tvs, +pi)) = { '$\pi$' };
set( ...
    [haxx.ZAxis] ...
  , 'TickLabel', tvl ...
);

% Prepare figure for export
textinterpreters(hf, 'latex');
fbaspect(hf, [3, 2]);
drawnow();

% Name where to save file to
hfname = sprintf('controller-error_angular-evolution_%s.%%1$s', datestr8601());
fpath = fullfile(bfolder(mfile), hfname);

tro.publish.savefig(hf, fpath, 'T-RO Figure');


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
