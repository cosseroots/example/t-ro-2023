function s = load(mfile)
%% LOAD
%
% Inputs:
%
%   MFILE                   Description of argument MFILE
%
% Outputs:
%
%   S                       Description of argument S
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% LOAD(MFILE)
narginchk(1, 1);

% LOAD(___)
% S = LOAD(___)
nargoutchk(0, 1);



%% Algorithm

% Create a MAT-file object to inspect the version number before loading all data
mobj = matfile(mfile);

v = [];

% Get VERSION field from loaded file
if isprop(mobj, 'version')
  v = mobj.version;
  
% No version yet flagged, so we will default to version 1
else
  v = 'v1';
  
end

% Function handle to version specific loading function
fun = str2func([ 'loadResult' , upper(v) ]);

% Check that the version number is not empty and there exists a callback
% function to load data for it
if isempty(v) || ~isfunction(fun)
  error('Unknown version number %s in simulation data file.', v);
end

% Load with the correctly dispatched version number
s = fun(load(mfile));

% TRO.LOAD(___)
if nargout == 0
  % Loop over all of the structure's fields and assign it in the caller's
  % workspace
  fn = fieldnames(s);
  nfn = numel(fn);
  for ifn = 1:nfn
    assignin('caller', fn{ifn}, s.(fn{ifn}));
    
  end
  
end


end


function matsout = loadResultV1(matsin)
%% LOADRESULTV1



matsout = matsin;

matsout.etaD = [];
matsout.etaE = [];


end


function matsout = loadResultV2(matsin)
%% LOADRESULTV2



matsout = matsin;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
