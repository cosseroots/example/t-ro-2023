function vc = body(w, d, h)
%% BODY
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% BODY(W, D, H)
narginchk(3, 3);

% BODY(___)
% VC = BODY(___)
nargoutchk(0, 1)



%% Algorithm

vc = { ...
    RigidBodyVisual.cuboid( ...
        w, d, h ...
      , 'Name'    , 'Shell' ...
      , 'Styling' , struct( ...
          'FaceColor', color('green') ...
        , 'EdgeColor', rgbdarker(color('green'), 0.1) ...
      ) ...
    ) ...
};

vc{1}.Styling.FaceColor = rgblighter(color('LightGray'), 0.50);
vc{1}.Styling.EdgeColor = rgbdarker(color('DarkGray'), 0.50);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
