function s = jointRod(varargin)
%% JOINTROD
%
% Outputs:
%
%   S                       Description of argument S
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% JOINTROD()
% JOINTROD(Name, Value, ...)
narginchk(0, Inf);

% JOINTROD(___)
% VC =JOINTROD(___)
nargoutchk(0, 1);



%% Algorithm

s = struct( ...
    'FaceColor', color('LightGray') ...
  , 'EdgeColor', color('DarkGray') ...
  , 'LineWidth', 0.50 ...
);

s = s + struct(varargin{:});


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
