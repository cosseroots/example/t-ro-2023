function hl = light(ax)
%% LIGHT
%
% Outputs:
%
%   HL                      Description of argument HL
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% LIGHT(AX)
narginchk(1, 1);

% LIGHT(___)
% HL = LIGHT(___)
nargoutchk(0, 1);


%% Algorithm

% Add an artifical light to the view
hl = light( ...
    ax ...
  , 'Position', [ 5 , 5 , 2 ] ...
  , 'Style', 'local' ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
