function vc = drone()
%% DRONE
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% DRONE()
narginchk(0, 0);

% DRONE(___)
% VC = DRONE(___)
nargoutchk(0, 1);



%% Algorithm

% Number of rotors: must be even
nr = 6;
vc = cell(nr, 2);

% Linear indices of rotor (zero-based)
idxp = (1:nr) - 1;

% Rotor dimensions
rotorDiameter = 100 * 1e-3;
rotorHeight   =   2 * 1e-3;
rotorOffsetZ  =   0 * 1e-3;
% Arm length
armWidth  = 150 * 1e-3;
armDepth  =   5 * 1e-3;
armHeight =  10 * 1e-3;

% Orientation of each rotor wrt frame base
yaw = linspace(0, 2 * pi, nr + 1) + pi / nr;
yaw(end) = [];
Rz = rotz(yaw);

% Orientation of each rotor wrt local frame (pitch/roll)
astar = deg2rad(26.5);
bstar = deg2rad(19.0);
% Sign of each rotor's rotation (forward or backward)
s = (-1) .^ idxp;
% Roll angles
roll = astar .* s;
% Pitch angles
pitch = bstar .* s;
% Yaw angles
yaw = 0 .* s;
% Euler angles for these parametrizations
%    = [   Z  ,   Y  ,   X  ]
euls = cat(1, yaw, pitch, roll);
% All rotation matrices
Rs = eul2rotm(euls, 'ZYX');

% Position vector of each rotor
pr  = [ armWidth ; 0 ; 0 ];
prr = [ 0 ; 0 ; rotorOffsetZ + 0.5 * rotorHeight ];
p = repmat( ...
    trvec(pr + prr) ...
  , [ 1 , nr ] ...
);
p = permute(pagemult(Rz, permute(p, [1, 3, 2])), [1, 3, 2]);

% Transformations for each rotor
Tr = tform(Rs, p);
Ta = tformmul(rotm2tform(Rz), trvec2tform([0;0;-0.5*armHeight]));

% Number of faces of each rotor **MUST BE EVEN**
nf = 34;
theta = linspace(0, 2 * pi, nf + 1);
theta = reshape(theta(1:end-1), [], 1);
% Vertices
v = cat( ...
    2 ...
  , rotorDiameter / 2 * cos(theta) ...
  , rotorDiameter / 2 * sin(theta) ...
  , -rotorHeight / 2 * ones(size(theta)) ...
);
v = cat( ...
    1 ...
  , v ... % bottom
  , v + [ 0 , 0 , rotorHeight ] ... % top
);

% Build faces
% Top and bottom are one face each
fb =      1:nf;
ft = nf + (1:nf);
% The sides are quadrilaterals
fs = transpose(cat(1, fb, circshift(fb, -1), circshift(ft, -1), ft));
fs = padarray(fs, [0, nf - size(fs, 2)], [[], NaN], 'post');
% And then there are the rotor blades
frb = ft(nf/2 + [0, nf/2]);
frb = padarray(frb, [0, nf - size(frb, 2)], [[], NaN], 'post');
% All faces of rotor
f = cat(1, fb, fs, ft, frb);

% Create each rotor
for ir = 1:nr
  % Rotor visual
  vc{ir,1} = RigidBodyVisual( ...
      'Name', sprintf('Rotor%d', ir) ...
    , 'Faces', f ...
    , 'Vertices', v ...
    , 'TForm', Tr(:,:,ir) ...
    , 'Styling', struct( ...
        'FaceColor', 0.666 * ones(1, 3) ...
      , 'FaceAlpha', 1.000 ...
    ) ...
  );
  
end

% Create legs for each rotor pair
for ir = 1:nr
  % Create vertices for arm
  
  % Faces for arm
  [f, v] = tro.meshgen.dronearm(armWidth, armDepth, armHeight);
  
  % And arm visual
  vc{ir,2} = RigidBodyVisual( ...
      'Name', sprintf('Leg%d(%d-%d)', ir, ir, ir + nr/2) ...
    , 'Faces', f ...
    , 'Vertices', v ...
    , 'TForm', Ta(:,:,ir) ...
    , 'Styling', struct( ...
        'FaceColor', 0.333 * ones(1, 3) ...
      , 'FaceAlpha', 1.000 ...
    ) ...
  );
  
end

% Remove any possibly empty visual elements
vc = transpose(vc(~cellfun(@isempty, vc)));

% Append cylinder for drone base
vc = [ ... 
    vc ...
  , { ...
      RigidBodyVisual.cylinder( ...
          0.1 * armWidth ...
        , 1.5 * armHeight ...
        , nr ...
        , 'Name', 'Body' ...
        , 'TForm', trvec2tform([0;0;-0.50*armHeight]) ...
        , 'Styling', struct( ...
            'FaceColor', 0.666 * ones(1, 3) ...
          , 'EdgeColor', 0.111 * ones(1, 3) ...
        ) ...
      ) ...
    } ...
];


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
