function animate(mfile, varargin)
%% ANIMATE
%
% Inputs:
%
%   MFILE                   Description of argument MFILE
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ANIMATE(MFILE)
% ANIMATE(MFILE, Name, Value)
narginchk(1, Inf);

% ANIMATE(___)
nargoutchk(0, 0);

% Find a possible axes object in the passed data
[cax, args, nargs] = axescheck(mfile, varargin{:});
mfile = args{1};
args(1) = [];

dopts = struct( ...
    'StartFcn'  , @noop ...
  , 'UpdateFcn' , @noop ...
  , 'StopFcn'   , @noop ...
);

uopts = nvpairs2struct(varargin{:});



%% Initialization

% Load data in a unified format
s = tro.load(mfile);

% Extract some data from MAT file
rbt   = s.smms;
% ngn   = s.engine;

t    = s.result.t;
q    = s.result.q;
fext = s.result.fext;



%% Animation

% Prepare a good AXES object
if isempty(cax) || ishghandle(cax, 'axes')
  cax = newplot(cax);
else
  cax = ancestor(cax, 'axes');
end

hold(cax, 'on');
coHold = onCleanup(@() cleanupAxis(cax));

% Push user-defined callback functions into the axes user data
cax.UserData.StartFcn  = optsget(uopts, 'StartFcn', dopts.StartFcn, 'fast');
cax.UserData.UpdateFcn = optsget(uopts, 'UpdateFcn', dopts.UpdateFcn, 'fast');
cax.UserData.StopFcn   = optsget(uopts, 'StopFcn', dopts.StopFcn, 'fast');

% Dispatch to animation
animate( ...
    rbt ...
  , cax ...
  , t, q ...
  , fext ...
  , 'StartFcn'  , @(ax, t_, q_) tro.animate.start(ax, rbt, t_, q_, fext(:,:,t2ind(t, t_))) ...
  , 'UpdateFcn' , @(ax, t_, q_) tro.animate.update(ax, rbt, t_, q_, fext(:,:,t2ind(t, t_))) ...
  , 'StopFcn'   , @(ax, t_, q_) tro.animate.stop(ax, rbt, t_, q_, fext(:,:,t2ind(t, t_))) ...
);


end


function cleanupAxis(ax)
%% CLEANUPAXIS



if ~isempty(ax) && ishandle(ax) && isvalid(ax)
  hold(ax, 'off');
end


end


function x = t2ind(t, tq)
%% T2IND
%
% Work around since simulation sample rate is probably different from the
% animation sample rate. Thus, the array of external forces is not the right
% size when in the animation callbacks. As such, we must find out which page of
% FEXT is associated with the current time index



[~, x] = min(abs(t - tq));
x = limit(x, 1, numel(t));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
