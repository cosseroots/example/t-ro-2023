function [f, v] = dronearm(w, d, h)
%% DRONEARM
%
% Inputs:
%
%   W                       Description of argument W
% 
%   D                       Description of argument D
% 
%   H                       Description of argument H
%
% Outputs:
%
%   F                       Description of argument F
% 
%   V                       Description of argument V
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% DRONEARM(WIDTH, DEPTH, HEIGHT)
narginchk(3, 3);

% [F, V] = DRONEARM(___)
nargoutchk(2, 2);



%% Algorithm

x1 = [ +0.00 , +0.10 , +0.40 , +0.75 , +1.00 , +1.02 ];
y1 = ones(size(x1));
z1 = [ +1.00 , +1.00 , +1.00 , +1.00 , +1.00 , +1.00 ];
zo = [ +0.00 , +0.00 , +0.20 , +0.20 , +0.00 , +0.00 ];

x = w     * [  x1 ,  x1 ,  x1 ,  x1 ];
y = d / 2 * [ +y1 , +y1 , -y1 , -y1 ];
z = h / 2 * [ +z1 , -z1 , +z1 , -z1 ] + 50 * h / 2 * [ -zo , -zo , -zo , -zo ];

v = transpose(cat(1, x, y, z));

f = [ ...
  ... % left
   1 ,  2 ,  8 ,  7 ; ...
   2 ,  3 ,  9 ,  8 ; ...
   3 ,  4 , 10 ,  9 ; ...
   4 ,  5 , 11 , 10 ; ...
   5 ,  6 , 12 , 11 ; ...
  ... % right (left + 12)
  13 , 14 , 20 , 19 ; ...
  14 , 15 , 21 , 20 ; ...
  15 , 16 , 22 , 21 ; ...
  16 , 17 , 23 , 22 ; ...
  17 , 18 , 24 , 23 ; ...
  ... % top
   1 ,  2 , 14 , 13 ; ...
   2 ,  3 , 15 , 14 ; ...
   3 ,  4 , 16 , 15 ; ...
   4 ,  5 , 17 , 16 ; ...
   5 ,  6 , 18 , 17 ; ...
  ... % bottom (top + 6)
   7 ,  8 , 20 , 19 ; ...
   8 ,  9 , 21 , 20 ; ...
   9 , 10 , 22 , 21 ; ...
  10 , 11 , 23 , 22 ; ...
  11 , 12 , 24 , 23 ; ...
  ... % caps
   6 , 12 , 24 , 18 ; ...
   1 ,  7 , 19 , 13 ; ...
];


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
