function hf = drawBodyTrajectory(rbt, ax, q)
%% DRAWBODYTRAJECTORY
%
% Inputs:
%
%   RBT                     Description of argument RBT
% 
%   AX                      Description of argument AX
% 
%   Q                       Description of argument Q
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% DRAWBODYTRAJECTORY(ROBOT, AX, Q)
narginchk(3, 3);

% DRAWBODYTRAJECTORY(___)
% HF = DRAWBODYTRAJECTORY(___)
nargoutchk(0, 1);



%% Algorithm

% Get position of each joint
T = reconstructKinematics(rbt, q);

% HG group for all external forces
hf = hggroup( ...
    ax ...
  , 'Tag', 'BodyTrajectories' ...
);

% Indices of body motion to plot
ind = 1:rbt.NumBody;

c = [ ...
  color('red')   ; ...
  color('green') ; ...
  color('blue')  ; ...
];

% Loop over each body index
nb = numel(ind);
for ii = 1:nb
  % Get body index
  ib = ind(ii);
  
  % Get global body position
  p = tform2trvec(T(:,:,ib));
  
  % Plot position
  hf_ = plot3( ...
      ax ...
    , p(1), p(2), p(3) ...
    , 'Color'       , c(ib,:) ...
    , 'LineStyle'   , '-' ...
    , 'Tag'         , sprintf('BodyTrajectory%d', ib) ...
  );
  
  % Assign parents of plot object
  set(hf_, 'Parent', hf);
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
