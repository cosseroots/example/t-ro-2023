function stop(ax, rbt, t, q, fext)
%% STOP
%
% Inputs:
%
%   AX                      Description of argument AX
% 
%   RBT                     Description of argument RBT
% 
%   T                       Description of argument T
% 
%   Q                       Description of argument Q
% 
%   FEXT                    Description of argument FEXT
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% STOP(AX, ROBOT, T, Q, FEXT)
narginchk(5, 5);

% STOP(___)
nargoutchk(0, 0);



%% Algorithm

if ~isvalid(ax)
  return
end

% Update additional drawings
tro.animate.redrawFext(                rbt, ax.UserData.ForcesGroup                 , q, fext);
tro.animate.redrawBodyTrajectory(      rbt, ax.UserData.BodyTrajectoriesGroup       , q);
tro.animate.redrawBodyTrajectoryShadow(rbt, ax.UserData.BodyTrajectoriesShadowsGroup, q);

% Call user-defined start function
ax.UserData.StopFcn(ax, t, q);

% Update drawing
drawnow();

% Unhold axes after animation
hold(ax, 'off');


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
