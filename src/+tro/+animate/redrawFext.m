function redrawFext(rbt, hg, q, fext)
%% REDRAWFEXT
%
% Inputs:
%
%   RBT                     Description of argument RBT
% 
%   AX                      Description of argument AX
% 
%   Q                       Description of argument Q
% 
%   FEXT                    Description of argument FEXT
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% REDRAWFEXT(ROBOT, HG, Q, FEXT)
narginchk(4, 4);

% REDRAWFEXT(___)
nargoutchk(0, 0);



%% Algorithm

% Get position of each joint
T = reconstructKinematics(rbt, q);

% Loop over each body
nb = rbt.NumBody;
for ib = 1:nb
  % Plot force part of force
  hf_ = findobj( ...
      hg ...
    , 'Tag', sprintf('F^ext_{%.0f}', ib) ...
  );
  
  % Push force to right place
  set( ...
      hf_ ...
    , 'Matrix', T(:,:,ib) ...
  );
  
  % Get force
  fext_ = fext(4:6,ib);
  % Its norm
  fext_n = norm(fext_);
  % And its normalize direction
  if fext_n > 0
    fext_ = fext_ ./ fext_n;
  end
  
  % Update quiver with new direction and new line width
  set( ...
    findobj( ...
        hf_ ...
      , 'Tag', sprintf('N^{ext}_{%.0f}', ib) ...
    ) ...
    , 'XData', [ 0 , fext_(1) ] ...
    , 'YData', [ 0 , fext_(2) ] ...
    , 'ZData', [ 0 , fext_(3) ] ...
    , 'LineWidth', exp(fext_n ^ (1 /4)) / 2 ...
  );
    
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
