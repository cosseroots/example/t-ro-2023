function start(ax, rbt, t, q, fext)
%% START
%
% Inputs:
%
%   AX                      Description of argument AX
% 
%   RBT                     Description of argument RBT
% 
%   T                       Description of argument T
% 
%   Q                       Description of argument Q
% 
%   FEXT                    Description of argument FEXT
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% START(AX, ROBOT, T, Q, FEXT)
narginchk(5, 5);

% START(___)
nargoutchk(0, 0);



%% Algorithm

if ~isstruct(ax.UserData)
  ax.UserData = struct();
end

hf = ancestor(ax, 'figure');

% General styling
viz.style.smms(ax);

% Axes labels
ax.XAxis.Label.String = '$x \left[ \mathrm{m} \right]$';
ax.YAxis.Label.String = '$y \left[ \mathrm{m} \right]$';
ax.ZAxis.Label.String = '$z \left[ \mathrm{m} \right]$';

% Set figure to 1280px width and 16:10 ratio
hf.Position(3) = 1280;
fbaspect(hf, 16 / 10);
% Disable resizing so video export won't error out
hf.Resize = 'off';

% Styling for high quality videos
ax.View = [ 135 , 10 ];

% Set camera projection to `perspective`
ax.Projection = 'perspective';

% Add light source
ax.UserData.Light = tro.visual.light(ax);

% Set all material properties to be dull
material('dull');

% Go through every graphics object so far and set its line width to half the
% original size (this will be rigid bodies and rigid/elastic joints)
c = findall(ax.Children);
nc = numel(c);
for ic = 1:nc
  c_ = c(ic);
  if isprop(c_, 'LineWidth')
    c_.LineWidth = 0.5 * c_.LineWidth;
  end
end

% Draw forces
ax.UserData.ForcesGroup                  = tro.animate.drawFext(rbt, ax, q, fext);
ax.UserData.BodyTrajectoriesGroup        = tro.animate.drawBodyTrajectory(rbt, ax, q);
ax.UserData.BodyTrajectoriesShadowsGroup = tro.animate.drawBodyTrajectoryShadow(rbt, ax, q);

% Call user-defined start function
ax.UserData.StartFcn(ax, t, q);

% Update drawing
drawnow();


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
