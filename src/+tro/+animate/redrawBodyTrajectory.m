function redrawBodyTrajectory(rbt, hg, q)
%% REDRAWBODYTRAJECTORY
%
% Inputs:
%
%   RBT                     Description of argument RBT
% 
%   AX                      Description of argument AX
% 
%   Q                       Description of argument Q
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% REDRAWBODYTRAJECTORY(ROBOT, HG, Q)
narginchk(3, 3);

% REDRAWBODYTRAJECTORY(___)
nargoutchk(0, 0);



%% Algorithm

% Get position of each joint
T = reconstructKinematics(rbt, q);

% Indices of body motion to plot
ind = 1:rbt.NumBody;

% Loop over each body index
nb = numel(ind);
for ii = 1:nb
  % Get body index
  ib = ind(ii);
  
  % Get global body position
  p = tform2trvec(T(:,:,ib));
  
  % Plot force part of force
  hf_ = findobj( ...
      hg ...
    , 'Tag', sprintf('BodyTrajectory%d', ib) ...
  );
  
  % Update coordinates of plot
  set( ...
      hf_ ...
    , 'XData', [ get(hf_, 'XData') , p(1,:) ] ...
    , 'YData', [ get(hf_, 'YData') , p(2,:) ] ...
    , 'ZData', [ get(hf_, 'ZData') , p(3,:) ] ...
  );
    
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
