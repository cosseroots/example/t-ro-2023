function [x, y, z] = viewedaxesplanes(ax)
%% VIEWEDAXESPLANES
%
% Outputs:
%
%   X                       Description of argument X
% 
%   Y                       Description of argument Y
% 
%   Z                       Description of argument Z
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% VIEWEDAXESPLANES()
% VIEWEDAXESPLANES(AX)
narginchk(0, 1);

% [X, Y, Z] = VIEWEDAXESPLANES(___)
nargoutchk(3, 3);

% VIEWEDAXESPLANES()
if nargin < 1 || isempty(ax)
  ax = gca();
end



%% Algorithm

% Get azimuth and elevation of camera to correctly set the shadow planes
[az, el] = view(ax);

% If elevation is positive, we will always have ZMIN as Z0, otherwise ZMAX as Z0
if el <= 0
  z = max(ax.ZAxis.Limits);

else
  z = min(ax.ZAxis.Limits);

end

az = wrapto360(az);

% Check the azimuth angle i.e., the angle wrt the negative Y axis
if       0 < az && az <=  90
  x = min(ax.XAxis.Limits);
  y = max(ax.YAxis.Limits);

elseif  90 < az && az <= 180
  x = min(ax.XAxis.Limits);
  y = min(ax.YAxis.Limits);

elseif 180 < az && az <= 270
  x = max(ax.XAxis.Limits);
  y = min(ax.YAxis.Limits);

else % 270 < az && az <= 360
  x = max(ax.XAxis.Limits);
  y = max(ax.YAxis.Limits);

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
