function update(ax, rbt, t, q, fext)
%% UPDATE
%
% Inputs:
%
%   AX                      Description of argument AX
% 
%   RBT                     Description of argument RBT
% 
%   T                       Description of argument T
% 
%   Q                       Description of argument Q
% 
%   FEXT                    Description of argument FEXT
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% UPDATE(AX, ROBOT, T, Q, FEXT)
narginchk(5, 5);

% UPDATE(___)
nargoutchk(0, 0);



%% Algorithm

% Update additional drawings
tro.animate.redrawFext(                rbt, ax.UserData.ForcesGroup                 , q, fext);
tro.animate.redrawBodyTrajectory(      rbt, ax.UserData.BodyTrajectoriesGroup       , q);
tro.animate.redrawBodyTrajectoryShadow(rbt, ax.UserData.BodyTrajectoriesShadowsGroup, q);

% Call user-defined start function
ax.UserData.UpdateFcn(ax, t, q);

% Get axes limits
axl = reshape(axis(ax), 2, []);
% Set axes limits to auto
axis(ax, 'tight');
axis(ax, 'padded');
% Get new axes limits
axlc = reshape(axis(ax), 2, []);
% Find the minimum/maximum between old and new values
axln = [ min([axl;axlc], [], 1) ; max([axl;axlc], [], 1) ];
% Set auto-calculated limits
axis(ax, reshape(axln, 1, []));

% Update drawing
drawnow();


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
