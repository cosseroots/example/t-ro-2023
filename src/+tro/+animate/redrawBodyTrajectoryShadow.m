function redrawBodyTrajectoryShadow(rbt, hg, q)
%% REDRAWBODYTRAJECTORYSHADOW
%
% Inputs:
%
%   RBT                     Description of argument RBT
% 
%   AX                      Description of argument AX
% 
%   Q                       Description of argument Q
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% REDRAWBODYTRAJECTORYSHADOW(ROBOT, HG, G)
narginchk(3, 3);

% REDRAWBODYTRAJECTORYSHADOW(___)
nargoutchk(0, 0);



%% Algorithm

% Get parent's axes object
ax = ancestor(hg, 'axes');

% Get the lowest value of each viewable axes plane
[x0, y0, z0] = tro.animate.viewedaxesplanes(ax);

% Get all body trajectories
hb = findobj(ax, 'Tag', 'BodyTrajectories');

% Loop over all body trajectories
for ii = 1:numel(hb.Children)
  % Reference graphics object
  hb_ = findobj(hb, 'Tag', sprintf('BodyTrajectory%d', ii));
  
  x_ = get(hb_, 'XData');
  y_ = get(hb_, 'YData');
  z_ = get(hb_, 'ZData');
  
  d = { ...
    { x_ , y_ , z0 .* ones(size(z_)) } ...
    { x0 .* ones(size(x_)) , y_ , z_ } ...
    { x_ , y0 .* ones(size(y_)) , z_ } ...
  };
  
  % Holds all three shadows
  hg_ = findobj(hg, 'Tag', sprintf('BodyTrajectoryShadow%d', ii));
  
  % Loop over all three planes XY, YZ, XZ
  for ix = 1:3
    % Get plot data
    d_ = d{ix};
    hf__ = findobj(hg_, 'Tag', sprintf('Shadow%d', ix));
    set( ...
      hf__ ...
      , 'XData', d_{1} ...
      , 'YData', d_{2} ...
      , 'ZData', d_{3} ...
    );
  end
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
