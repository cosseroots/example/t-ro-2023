function hf = drawBodyTrajectoryShadow(rbt, ax, q)
%% DRAWBODYTRAJECTORYSHADOW
%
% Inputs:
%
%   RBT                     Description of argument RBT
% 
%   AX                      Description of argument AX
% 
%   Q                       Description of argument Q
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% DRAWBODYTRAJECTORYSHADOW(ROBOT, AX, Q)
narginchk(3, 3);

% DRAWBODYTRAJECTORYSHADOW(___)
% HF = DRAWBODYTRAJECTORYSHADOW(___)
nargoutchk(0, 1);



%% Algorithm

% Get the lowest value of each viewable axes plane
[x0, y0, z0] = tro.animate.viewedaxesplanes(ax);

hf = hggroup( ...
    ax ...
  , 'Tag', 'BodyTrajectoriesShadows' ...
);

% Get all body trajectories
hb = findobj(ax, 'Tag', 'BodyTrajectories');

% Loop over all body trajectories
for ii = 1:numel(hb.Children)
  % Reference graphics object
  hb_ = findobj(hb, 'Tag', sprintf('BodyTrajectory%d', ii));
  
  % Holds all three shadows
  hf_ = hggroup( ...
      ax ...
    , 'Tag', sprintf('BodyTrajectoryShadow%d', ii) ...
  );
  
  x_ = get(hb_, 'XData');
  y_ = get(hb_, 'YData');
  z_ = get(hb_, 'ZData');
  
  d = { ...
    { x_ , y_ , z0 .* ones(size(z_)) } ...
    { x0 .* ones(size(x_)) , y_ , z_ } ...
    { x_ , y0 .* ones(size(y_)) , z_ } ...
  };
  lii = { ...
     'on' ,  'on' , 'off' ; ...
    'off' ,  'on' ,  'on' ; ...
     'on' , 'off' ,  'on' ; ...
  };
  
  % Loop over all three planes XY, YZ, XZ
  for ix = 1:3
    % Get plot data
    d_ = d{ix};
    c = get(hb_, 'Color');
    % And plot it
    hf__ = plot3( ...
        ax ...
      , d_{:} ...
      , 'LineStyle'   , '-' ...
      , 'Color'       , rgblighter(c, 0.55) ...
      , 'LineWidth'   , 0.5 * get(hb_, 'LineWidth') ...
      , 'Tag'         , sprintf('Shadow%d', ix) ...
      , 'Clipping'    , 'off' ...
      , 'XLimInclude' , lii{ix,1} ...
      , 'YLimInclude' , lii{ix,2} ...
      , 'ZLimInclude' , lii{ix,3} ...
    );
    
    set(hf__, 'Parent', hf_);

  end
  
  set(hf_, 'Parent', hf);
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
