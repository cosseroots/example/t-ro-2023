function exportsnapshot(hf, fpath)
%% EXPORTSNAPSHOT
%
% Inputs:
%
%   HF                      Description of argument HF
%
%   T                       Description of argument T
% 
%   FPATH                   Description of argument FPATH
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% EXPORTSNAPSHOT(HF, FPATH)
narginchk(2, 2);

% EXPORTSNAPSHOT(___)
nargoutchk(0, 0);



%% Export

% Get current axes from current figure
ax = findobj(hf, 'type', 'axes');

% Create a copy of the figure
hf_ = figure();
copyobj(ax, hf_);
ax_ = findobj(hf_, 'Type', 'axes');
coFig = onCleanup(@() close(hf_));

% Clean up figure:
% Remove force arrows
delete(findobj(ax_, 'Tag', 'BodyTrajectoriesShadows'));
% Remove shadow trajectories
delete(findobj(ax_, 'Tag', 'ExternalForces'));

% Set figure's aspect ratio
fbaspect(hf_, [ 2 , 1 ]);

set(hf_, 'Renderer', 'Painters');

% Export figure in original format
saveas( ...
    hf_ ...
  , ensuredir(sprintf(fpath, 'fig')) ...
);

% Export figure for paper
hgexport( ...
    hf_ ...
  , 'temp_dummy' ...
  , hgexport('readstyle', 'T-RO FullFigure One Half') ...
  , 'applystyle', true ...
);
drawnow();

exportgraphics( ...
    hf_ ...
  , ensuredir(sprintf(fpath, 'pdf')) ...
  , 'ContentType', 'vector' ...
  , 'BackgroundColor', 'none' ...
  , 'Resolution', 150 ...
);
exportgraphics( ...
    hf_ ...
  , ensuredir(sprintf(fpath, 'eps')) ...
  , 'ContentType', 'vector' ...
  , 'BackgroundColor', 'none' ...
  , 'Resolution', 150 ...
);
exportgraphics( ...
    hf_ ...
  , ensuredir(sprintf(fpath, 'png')) ...
  , 'ContentType', 'vector' ...
  , 'BackgroundColor', 'none' ...
  , 'Resolution', 150 ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
