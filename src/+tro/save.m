function mfile = save(rbt, engine, t, q, qdot, qddot, fext, etaD, etaE)
%% SAVE
%
% Inputs:
%
%   RBT                     Description of argument RBT
% 
%   ENGINE                  Description of argument ENGINE
% 
%   T                       Description of argument T
% 
%   Q                       Description of argument Q
% 
%   QDOT                    Description of argument QDOT
% 
%   QDDOT                   Description of argument QDDOT
% 
%   FEXT                    Description of argument FEXT
% 
%   ETAD                    Description of argument ETAD
% 
%   ETAE                    Description of argument ETAE
% 
% Outputs:
%
%   MFILE                   Description of argument MFILE
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% SAVE(ROBOT, ENGINE, T, Q, QDOT, QDDOT, FEXT, ETAD, ETAE)
narginchk(8, 9);

% MFILE = SAVE(___)
nargoutchk(0, 1);



%% Algorithm

% Version of stored file
SVERSION = 2;

% Date to indicate when file was created
d = datestr8601();

% Path of resulting MAT file
mfile = ensuredir(data(MatlabProject.this(), 'mat', sprintf('%s.mat', d)));

% Build structure of data to save
s = struct( ...
    'version' , sprintf('v%0.f', SVERSION) ...
  , 'date'    , d ...
  , 'smms'    , rbt ...
  , 'engine'  , engine ...
  , 'result'  , struct( ...
      't'         , t ...
    , 'q'         , q ...
    , 'qdot'      , qdot ...
    , 'qddot'     , qddot ...
    , 'fext'      , fext ...
    , 'etaD'      , etaD ...
    , 'etaE'      , etaE ...
  ) ...
);

save( ...
    mfile ...
  , '-struct', 's' ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
